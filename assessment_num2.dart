void main() {
  //create an array to store all the winning apps of the MTN Business App of the year Awards since 2017
  Map app_Names = {
    'Ambani': 2021,
    'EasyEquitities': 2020,
    'NakedInsurance': 2019,
    'Khula': 2018,
    'StandardBankShyft': 2017
  }; // app_Names.add('fnb');
  //a) sort and print the apps by name;
  // 1.for Each  through the collection
  //loop
  app_Names.forEach((key, value) {
    // We test a condition
    if (value == 2017 || value == 2018) {
      //we print the name of the apps
      print("Name of the app :$key");
    } /*else {
      print("no such values exist inside the collection ");
    }*/
  });
  // the print total number of apps from the array.
  int count = 0;
  for (var name = 1; name <= app_Names.length; name++) count++;
  {
    print("Total number of the app is: $count");
  }
}
