//Write a basic program that stores and then prints the following data:
//Your name, favorite app, and city;

void main() {
  String fullName = "Thilangiwi Tshikotshi";
  String favoriteApp = "standard banking";
  String city = "Johannesburg";
  String message =
      ('My name is $fullName.\nI stay in $city, and my favorite app is $favoriteApp. ');

  print(message);
  print('*' * 140);
  var message_0 = message;
  // ignore: unused_local_variable
  var favorite_App = 'shyft for standard bank';
  city = 'pretoria';
  print(message_0);
}
