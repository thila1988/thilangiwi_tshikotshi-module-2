//create a class and
//a) then use an object to print the name of the app, sector/category, developer, and the year

void main() {
  WinningApp winner = new WinningApp('appName', 'category', 'developer', 0000);

  winner._appName = 'standard banking';
  winner._category = 'finance best solution';
  winner._developer = 'Langi';
  winner._year = 2018;

  print(winner.upperCase('${winner._appName}'));
  print(winner.message());
}

class WinningApp {
  String _appName = "";

  // ignore: unused_field
  String _category = '';

  String _developer = "";

  int _year = 0;

  WinningApp(String appName, String category, String developer, int year) {
    this._appName = appName;
    this._category = category;
    this._developer = developer;
    this._year = year;
  }
  // create a function inside the class, transform  the app name
  //to all capital letters and then print the output
  String upperCase(String appName) {
    appName = this._appName.toUpperCase();
    return appName;
  }

  String message() {
    String message =
        '${upperCase(this._appName)}, it is been the best ${this._category} of mtn Business app of ${this._year} and it been developed by ${this._developer}';
    return message;
  }
}
